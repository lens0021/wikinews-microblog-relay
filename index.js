#!/usr/local/bin/node
import webcrypto from 'crypto';
import dotenv from 'dotenv';
import express from 'express';
import MWBot from 'mwbot';
import rssParser from 'rss-parser';
import superagent from 'superagent';
import superagentJsonapify from 'superagent-jsonapify';
import { Client, auth } from 'twitter-api-sdk';

const wikinewsMicroblogRelayVarPrefix = 'WIKINEWS_MICROBLOG_RELAY_VAR_';
const scopes = [
  'tweet.read',
  'users.read',
  'tweet.write',
  // request refresh token
  'offline.access',
];

dotenv.config();
superagentJsonapify(superagent);

const authClient = new auth.OAuth2User({
  client_id: process.env.CLIENT_ID,
  client_secret: process.env.CLIENT_SECRET,
  callback: `${process.env.SERVER}/callback`,
  scopes,
});

const app = express();
let STATE = process.env.STATE;
if (!STATE) {
  STATE = await getTfcVariable('STATE');
  if (!STATE) {
    STATE = generateRandomString();
    await setTfcVariable('STATE', STATE);
  }
}

function generateRandomString() {
  const arr = new Uint32Array(28);
  webcrypto.getRandomValues(arr);
  return Array.from(arr, (dec) => ('0' + dec.toString(16)).substring(-2)).join(
    ''
  );
}

async function getTfcVariable(name) {
  const res = await superagent
    .get('https://app.terraform.io/api/v2/vars')
    .query({
      'filter[organization][name]': 'lens0021',
      'filter[workspace][name]': 'oci',
    })
    // .set('Content-Type', 'application/json')
    .set('Authorization', `Bearer ${process.env.TERRAFORM_CLOUD_TOKEN}`);
  return res.body?.data.filter((v) => {
    return v.key === wikinewsMicroblogRelayVarPrefix + name;
  })[0].value;
}

async function getTfcVariableId(name) {
  const res = await superagent
    .get('https://app.terraform.io/api/v2/vars')
    .query({
      'filter[organization][name]': 'lens0021',
      'filter[workspace][name]': 'oci',
    })
    // .set('Content-Type', 'application/json')
    .set('Authorization', `Bearer ${process.env.TERRAFORM_CLOUD_TOKEN}`);
  return res.body?.data.filter((v) => {
    return v.key === wikinewsMicroblogRelayVarPrefix + name;
  })[0].id;
}

async function setTfcVariable(name, value) {
  const variableId = await getTfcVariableId(name);
  await superagent
    .patch(`https://app.terraform.io/api/v2/vars/${variableId}`)
    .send({
      data: {
        attributes: {
          value,
        },
      },
    })
    .set('Content-Type', 'application/vnd.api+json')
    .set('Authorization', `Bearer ${process.env.TERRAFORM_CLOUD_TOKEN}`);
}

app.get('/login', async function (req, res) {
  const authUrl = authClient.generateAuthURL({
    state: STATE,
    code_challenge_method: 's256',
  });
  res.redirect(authUrl);
});

app.get('/', async function (req, res) {
  let body = '';

  body += ['login', 'tweet']
    .map((route) => `<a href="/${route}">${route}</a>`)
    .join(' · ');
  const SHOW_CREDENTIALS = false;
  body += `LANG=${process.env.LANG}<br>`;
  if (SHOW_CREDENTIALS) {
    body +=
      '<ul><li>' +
      [
        // TODO: avoid sequential calls
        `accessToken: ${await getTfcVariable('ACCESS_TOKEN')}`,
        `refreshToken: ${await getTfcVariable('REFRESH_TOKEN')}`,
        `expiresAt: ${await getTfcVariable('EXPIRES_AT')}`,
      ].join('</li><li>') +
      '</li></ul>';
  }
  res.send(body);
});

app.get('/callback', async function (req, res) {
  try {
    const { code, state } = req.query;
    if (state !== STATE) return res.status(500).send("State isn't matching");

    await authClient.requestAccessToken(code); // Move to tweet
    await setTfcVariable('ACCESS_TOKEN', authClient.token.access_token);
    await setTfcVariable('REFRESH_TOKEN', authClient.token.refresh_token);
    await setTfcVariable('EXPIRES_AT', authClient.token.expires_at.toString());
    res.send('Authentication done\n');
  } catch (error) {
    console.log(error);
  }
});

app.get('/tweet', async function (req, res) {
  const query = Object.entries({
    title: 'Special:NewsFeed',
    feed: 'atom',
    categories: encodeURIComponent('발행됨'),
    notcategories: encodeURIComponent('보존됨'),
    namespace: 0,
    count: 10,
    ordermethod: 'categoryadd',
    stablepages: 'only',
  })
    .map(([k, v]) => k + '=' + v.toString())
    .join('&');

  const feed = await new rssParser().parseURL(
    `${process.env.MEDIAWIKI_API}?${query}`
  );

  const bot = new MWBot({
    apiUrl: 'https://ko.wikinews.org/w/api.php',
  });

  await bot.loginGetEditToken({
    username: process.env.MWBOT_USERNAME,
    password: process.env.MWBOT_PASSWORD,
  });

  const titles = feed.items.map((i) => i.title);
  const info = await bot.request({
    action: 'query',
    format: 'json',
    prop: 'info',
    titles: titles.join('|'),
    formatversion: '2',
    inprop: 'watched',
  });
  const watchedMap = {};
  const pageIdMap = {};
  for (const page of info.query.pages) {
    watchedMap[page.title] = page.watched;
    pageIdMap[page.title] = page.pageid;
  }

  try {
    const authClient = new auth.OAuth2User({
      client_id: process.env.CLIENT_ID,
      client_secret: process.env.CLIENT_SECRET,
      callback: `${process.env.SERVER}/callback`,
      scopes,
      token: {
        token_type: 'bearer',
        scope: scopes.join(','),
        access_token: await getTfcVariable('ACCESS_TOKEN'),
        refresh_token: await getTfcVariable('REFRESH_TOKEN'),
        expires_at: parseInt(await getTfcVariable('EXPIRES_AT')),
      },
    });
    const client = new Client(authClient);

    const VERIFY_ACCOUNT = false;
    if (VERIFY_ACCOUNT) {
      const me = await client.users.findMyUser();
      if (me.data.username != process.env.USER_NAME) {
        res.send(
          `Username is not ${process.env.USER_NAME} (${me.data.username})\n`
        );
        return;
      }
    }

    const newlyWatched = [];
    for (const title of titles.reverse()) {
      if (watchedMap[title]) {
        continue;
      }

      console.log(
        `${title} https://ko.wikinews.org/?curid=${pageIdMap[title]}`
      );
      await client.tweets.createTweet({
        text: `${title} https://ko.wikinews.org/?curid=${pageIdMap[title]}`,
      });
      newlyWatched.push(pageIdMap[title]);
    }

    // Watch
    if (newlyWatched) {
      const watchToken = (
        await bot.request({
          action: 'query',
          meta: 'tokens',
          type: 'csrf|watch',
        })
      ).query.tokens.watchtoken;

      await bot.request({
        action: 'watch',
        token: watchToken,
        format: 'json',
        pageids: newlyWatched.join('|'),
        formatversion: '2',
      });
    }

    res.send(`Tweet done\n`);
  } catch (error) {
    // Send notification
    await superagent.post(process.env.DISCORD_WEBHOOK).send({
      content: `error: ${JSON.stringify(error.error)}`,
    });

    console.log('Error: ', error.error || error.response?.error || error);
  }
});

app.listen(3000, () => {
  console.log(`Go here: ${process.env.SERVER}`);
});
