FROM node:20

RUN apt-get update && apt-get -y install cron

WORKDIR /srv/wikinews-microblog-relay
COPY . .

RUN npm install --production

# Set up cron
RUN echo "*/5 * * * * curl -s 127.0.0.1:3000/tweet > /proc/1/fd/1 2>&1" > crontab &&\
  crontab crontab &&\
  rm crontab

EXPOSE 3000

# The default is maybe `node`. Overwrite it.
ENTRYPOINT []

CMD ["/bin/bash", "/srv/wikinews-microblog-relay/docker-cmd"]
