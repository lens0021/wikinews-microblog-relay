# wikinews-microblog-relay

## Usage

```sh
docker pull registry.gitlab.com/lens0021/wikinews-microblog-relay
docker run \
  --name wikinews-microblog-relay \
  --rm \
  -e 'CLIENT_ID=b2hEaWg4VFpua1JYVFRWbVlRdGU6MTpjaQ' \
  -e 'CLIENT_SECRET=xxxxxx' \
  -e 'USER_NAME=wikinews_ko2022' \
  -e 'SERVER=http://123.123.123.123:3000' \
  -e 'TERRAFORM_CLOUD_TOKEN=xxxxxx' \
  -e 'DISCORD_WEBHOOK=xxx' \
  -e 'MEDIAWIKI_API=https://ko.wikinews.org/w/index.php' \
  -e 'MWBOT_USERNAME=Lens0021@wikinews-microblog-relay' \
  -e 'MWBOT_PASSWORD=xxx' \
  registry.gitlab.com/lens0021/wikinews-microblog-relay \
;
```
